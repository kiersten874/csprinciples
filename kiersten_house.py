from turtle import *
space = Screen() 
Kiersten = Turtle() 

# square
Kiersten.forward(100)
Kiersten.right(90)
Kiersten.forward(100)
Kiersten.right(90)
Kiersten.forward(100)
Kiersten.right(90)
Kiersten.forward(100)

# roof
Kiersten.right(90)
Kiersten.forward(100)
Kiersten.right(-120)
Kiersten.forward(100) 
Kiersten.right(-120)
Kiersten.forward(100)
Kiersten.right(-120)
Kiersten.forward(100) 
Kiersten.forward(-120)

#chimney
Kiersten.right(-60)
Kiersten.forward(40)
Kiersten.setheading(90)
Kiersten.color("red")
Kiersten.forward(30)
Kiersten.right(90) 
Kiersten.forward(30)
Kiersten.right(90)
Kiersten.forward(30)
Kiersten.right(90)

#window
Kiersten.penup()
Kiersten.goto(50,-50)
Kiersten.pendown()
Kiersten.setheading(90)
Kiersten.color("green")
Kiersten.forward(30)
Kiersten.right(90)
Kiersten.forward(30)
Kiersten.right(90)
Kiersten.forward(30)
Kiersten.right(90)
Kiersten.forward(30)


input()
