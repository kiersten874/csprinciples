# The code below is supposed to print an estimate of the square root. But, the indention is wrong on 4 lines. Fix it.

target = 6
guess = 2
guess_squared = guess * guess
while abs(target - guess_squared) > 0.01:
   closer = target / guess
   guess = (guess + closer) / 2.0
   guess_squared = guess * guess
print("Square root of", target,"is", guess)
