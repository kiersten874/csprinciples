# The program below is supposed to print the times tables for 1 to 3, but there are 5 errors. Fix the errors.


for x in range(1, 3):
    for y in range(1, 10):
        print(str(x) + " * " + str(y) + " = " +  str(x * y))
