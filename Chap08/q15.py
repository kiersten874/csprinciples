# Modify the code below to create a function that will take numbers as input until you enter a negative number and then will return the average of the numbers.


def calculateAverage():
   sum = 0
   count = 0
   message = "Enter an integer or a negative number to stop"
   value = input(message)
   while int(value) > 0:
      print("You entered " + value)
      sum = sum + int(value)
      count = count + 1
      value = input(message)
   return(sum / count)
print(calculateAverage())

