# Create a function to calculate and return the sum of all of the even numbers from 0 to the passed number (inclusive) using a while loop.

def calc_sum(lastnum):
    sum = 0
    num = 0
    while (num <= lastnum):
       sum = sum + num
       num = num + 2
    return sum
    
print(calc_sum(10))


