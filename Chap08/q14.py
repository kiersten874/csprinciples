# The code below currently enters a loop where it keeps printing “Even”. Fix the code so that it prints “Even” and “Odd” for numbers 0 to 9.

number = 0
while number < 10:
    while number % 2 == 0:
        print("even")
        number += 1  
    while number % 2 != 0:
        print("odd")
        number += 1
