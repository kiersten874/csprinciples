# Write a procedure that takes an int argument and uses a while loop to create a right-triangle like shape out of *. The first row should have 1 star and the last should have n stars where n is the argument passed.


def triangle(t):
    x = 1
    while x <= t:
        print("*" * x)
        x += 1
triangle(10)
