# Finish lines 1 and 5 so that the following code correct prints all the values from -5 to -1.

output = " "
x = -5
while x < 0:
    output = output + str(x) + " "
    x = x + 1
print(output)
