# The following code will loop infinitely. Make one change that will make it loop only 5 times.


x = 5
while x > 0:
    print(x)
    x = x - 1
