# Fix and change the code so it prints a table of division instead of multiplication for -10 to -1.

for x in range(-10,0):
   for y in range(1,11):
      print(str(x) + " / " + str(y) + " = " + str(x/y) )
