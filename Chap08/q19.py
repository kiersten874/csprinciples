# Create a procedure to print stars and spaces in a roughly square pattern and have it take as input the number of stars on a side. Use a nested loop to do this.

def inputFunc():
   stars = input("How many stars?")
   stars = int(stars)
   line = ""
   for x in range(0, stars):
      line = line + '* '
   for y in range(0,stars):
      print(line)
inputFunc()
