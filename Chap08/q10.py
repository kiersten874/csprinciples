# Rewrite the code that prints the times tables for 1 to 3 using a while loop and a for loop instead of two for loops.

x = 0
while x < 4:
    for y in range(1, 11):
          print(str(x) + " * " + str(y) + " = " + str(x * y))
    x += 1
