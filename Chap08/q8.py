# The function currently takes a start and stop argument and uses a for loop to find the sum of all the numbers between them (inclusive). Change the for loop to a while loop while still using the parameters.


def sum_func(start, stop):
    sum = 0
    num = start
    while num <= stop:
       sum = sum + num
       num = num + 1
    return sum


print(sum_func(1, 10)) 

