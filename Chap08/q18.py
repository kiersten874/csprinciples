# Write a procedure that takes a user input and keeps asking for a user input until the input is “Hello”. If the input is not “Hello”, it should print “This is your n wrong try.” where n is the number of times they have put an input in. If they type “Hello”, the procedure should print “Success!”. Hint: != means does not equal


def inputfunc():
    strinput = input("Type something")
    count = 0
    while strinput != "Hello":
       count += 1
       print("This is your " + str(count) + " wrong try.")
       strinput = input("Type something")
    print("Success!")
inputfunc()
