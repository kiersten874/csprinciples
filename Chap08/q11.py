# Rewrite the following code to use a while loop instead of a for loop.

product = 1  # Start out with nothing
number = 1
numbers = range(1,11)
while number < 11:
    product = product * number
    number = number + 1
print(product)

