# Presentation Notes

## Study Tips:

* know the 5 big ideas, study one at a time
* practice


## Resources:

* khan academy with quizzes 
* past years questions
* test guide website with practice tests



## Other
* the exam has a 66% pass rate
* ranked 2.8/10 on the diffucultly scale and 3.2/10 for the study time required.
* during the test, Save questions you dont know for last

## Practice questions:

### From pratice test 3:

You connect a new device to your Wi-Fi internet connection at home. What will happen next?

- [ ] The device will be assigned a new device driver 
- [ ] The device will be assigned a new packet number
- [ ] The device will be assigned a new Web address
- [x] The device will be assigned a new IP address

A certain bank uses an AI system to determine whether a customer can be given a loan or not. The AI uses attributes like age, employment status, and active-loan status to judge. This has caused very few loans to be given out to recent college graduates because of their existing college loans. Where does the bias exist within the data processing cycle? 
 
- [ ] Bias exists at the data collection stage
- [x] Bias exists at the data preparation stage
- [ ] Bias exists at the data analysis stage
- [ ] Bias exists at the data reporting stage

A private company employee needs to share some of the confidential files from his organization with an employee from another organization. However, the files cannot be directly sent, and there needs to be some encryption on the files to prevent unauthorized access from a third-party body. Which of the following encryption methods would offer the best encryption here? 

- [x] Public key encryption
- [ ] Certificate Authorities
- [ ] Symmetric key encryption
- [ ] Multi-factor authentication
