# Quiz Reflection Questions - Kiersten sproles


## Quiz 1 - 2 wrong

### 3. The figure below shows a circuit composed of two logic gates. The output of the circuit is true.
![image](IMG_5262.heic)

* I said input A must be false, which was the incorrect anwser. I selected this anwser because I got the 'and' and 'or' logic mixed up and interpreted the logic gates wrong.
* The correct anwser was input A must be true, because that would be the only way it could end up true with these logic gates.

### 8. You devolp a code that lets you use differnt operations on a set of numbers dervived from a database. However, the actual test results give a random value, which leads you to suspect that there is a randomizing variable in your code. What should you do to find the problem.

* My anwser was use a code visualizer. I selected this answer because I was unclear on what a code visualizer, so I assumed it was something that would visualize/show what is happening in each code segment, so I thought it was the soultion
* The correct answer is trace the code. It is correct because tracing the code would help you determine where the problem was. 

## Quiz 2 - 2 wrong

### 9. Using binary search, how many iterations  would it take to find the letter w?
* I chose the answer 23, because I did not know what binary search was, so I just counted which letter of the alphabet it was.
* The correct answer was 3. I now know that binary searh basically cuts the list in half, and then half again, until it finds what it is looking for. Therefore, it would cut the list in half 3 times (3 iterations) before it finds 'w'. 

### 10.  Using linear search, how many iterations  would it take to find the letter 'x'?
* I chose 23, because I miscounted which letetr fo the alphabet it is.
* The answer was 24, bc x is the 24th letter of the alphabet. I know know the linear search just searches from beggining to end in order.

## Quiz 3 - 3 wrong

### 3. Digital alarm clocks dislplay information and digital indicators to help people wake up on time. WHich of the indicators could represent a single bit of information?

* I said the current month and hour. This, however is wrong because they require multiple bits.
* The correct anwser is AM/PM indicatir and tempature unit indactor F/C because it can be stored in one bit.

### 6. The same task is peformed through sequential and parallel computing models. The time taken by the sequential setup was 30 seconds, while it took the parallel modele only 10 seeconds. What will be the speedup parameter for working with the parallel from now on?

* I chose 0.333, which is wrong. I had just misread the question.
* the answer is 3 because 10 seconds is 3x as fast as 30 seconds.

### 8. A small team of butterfly researchers are working on a project that monitors the sucess rate of catapillars to butterfly transistion. Which of the following best explains why citizen science is consindered useful for this project.
* I was supposed to select 2 awnsers but i forgot so iu only selected one. the awnser i selected was 'distributed individuals are more likely able to count a larger number of catapillars and butterflies over a larger areea than a small team of scientists.' I chose this answer because a group of individuals can do things faster than a small team of scientists, so it is useful.

* The correct anwsers are 

## Quiz 4 - 5 wrong

### 1. (Truth table) F , F -missing condition- F

* I chose the answer not(A or B) because i didnt understand the question and misinterpreted it.
* The correct anwnser was A and not(B). This is the answer because A and B are alse, so false and not(false) would end up false.

### 5.  condition database contains this info: name of artist, date of show, total dollar amount of all tickets sold. What additional pieces of info would be most useful in determinting the artist with the greatest attendnece.

* I chose total dollar amount of food and drinks sold, because I thought would be only way to show the populairity. however, i relize now that that wouldnt be as helpful because it could change based on differnt variables like show time.
* the correct anwser was average ticket price. By dividing the average ticket price by total money from ticket sales you can get the number of people that attended.

### 6. A camera mounted on the dashboard of a car captures an image of the view from the drivers every second. What can be determined with this data?

* i chose tyhe anwser "average number of hours per day the car is in use." I chose this answer because since a picture is taken every second, i thought you could take the total number of pictures and dived them by 60 to get minutes, then /60 again to get the hours. However, I realize now that it wouldnt work because the date is stored in the metadata so it wouldnt be able to determine the daily average. 
* The correct answer is # of bikes because you can look at that using only the data.

### 8. Which of the following best describes the behavior of the proceure? (question beneath code segment) 

* I chose the answers the program correctly displays the count if the target is not in the list and the program always displays the cirrect value for count. This is wrong because those are condracticing each other so that doesnt make any sense.
* the correct answers are b and c

### 9. Logic gate, find output
* i chose True when both and A and B are true only. I chose this because i didnt look at all the options and saw at first that true works, so i put this as the answer.
* however, the answer was always True regardless of the values of A and B. Looking back on it, i see that not only it works when both are true, but also if they are false.
