def double(stuff):
    """
     >>> double(5)
     10
     >>> double(7)
     14
    """
    return 10

if __name__ == "__main__":
    import doctest
    doctest.testmod()
