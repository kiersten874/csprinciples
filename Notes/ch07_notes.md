# Chapter 7- Computers Can Repeat Steps

## Repeating Steps

* for- used for a loop that repeats code
* range- used to create a list of numbers

A computer never gets tired; it can repeat code over and over and over as long as it has electricity.

## Repeating with numbers

* for loop- a type of loop to repeat statement(s) in a program. A for loop uses a variable and takes o each of the values of a list.
* A list holds values in a order.
* indented lines are mean't to stop it from running at the left boundrary,and so it starts under other code.
* indentions and ':' are both required for loops.

## What is a list?

* A List holds items
* closed by []
* seperated by commas
* A list has an order and every object has a posistion in that list

### Better Variable Names
* Instead of sum, use product


## The Range Function
* Used tooop over a sequence of numbers.
* If the range function is used with a single positive integer (ex: for number in range(4): ) it will generate all the integer values from 0 to one less than that number

## There's a Pattern Here!
* Steps of Accumulator pattern: 
1. Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
2. Get all the data to be processed.
3. Step through all the data using a for loop so that the variable takes on each value in the data.
4. Combine each piece of the data into the accumulator.
5. Do something with the result.

* The range function can be used to with patterns too. If you use 3 numbers, the first number is the starting number, the second number is the end number, and the third number is how how change between the numbers. (Example: for i in range(1, 11, 2):, will print 1, 3, 5, 7, 9)

*  How to add all numbers from 1-100 even:

![rangecodeimage](codech7range.jpg) 

## Adding print statements
* The goal of this stage in learning programming is being able to visualize what the code is going to print before you run it.

