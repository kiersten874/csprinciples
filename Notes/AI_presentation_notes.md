# AI-Enable Cyber Notes - Matt Mickelson

## Ways AI affecting technology
* behavior
* vision
* political
* social

## Bad ways AI affecting technology
* Automation of human activity
* targeting
* new threats
* ability to decieve in an automated way

## AI Reconigztion mistakes
* Ai can make mistakes and have ambigous results
* one example of this is how chiwawas and muffins get confused.

## Best models are..
* testable
* easy to debug
* verifable
* clear on what is a fault
