# Big Idea 1: Creative Development

## Main ideas

- Collaboration on projects with diverse team members representing a variety
  of backgrounds, experiences, and perspectives can develop a better computing
  product.
- A *development process* should be used to design, code, and implement
  software that includes using feedback, testing, and reflection.
- Software documentation should include the programs requirements, constraints,
  and purpose.
- Software should be adequately tested before it is released.
- *Debugging* is the process of finding and correcting errors in software.


## Vocabulary

- code segment - A segement of a code file.
- collaboration - working together/ made toghether 
- comments - usually opinions, compliments, or constructive criticism about someone's work. 
- debugging - searching and fixing possible errors.
- event-driven programming- a program written to respond to the user/system.
- incremental development process - breaking the software devolpmental process down into small, managable portions.
- iterative development process - breaking down the software process into smaller pieces.each piece is designed, tested, and devoloped.
- logic error - when their is a fault in the logic or algorithm.
- overflow error - when the data type being used to store the data isnt big enough.
- program - a series of coded software
- program behavior - a coding techique that used tthe icremenatl design process in a natrual way.
- program input - the user giving somethin to the computer
- program output - the computers response to the input
- prototype - a first model which other forms are devolped from.
- requirements - what is needed to complete the task.
- runtime error - an error that prevents the program from running.
- syntax error - a string or charcter in an incorrect place that prevents a successeful exuctuion of the code
- testing - checking/examining for errors.
- user interface - when a user and the computer interact. ex: computer mouse


## Computing Innovation

A *computer artifact* is anything created using a computer, including apps,
games, images, videos, audio files, 3D-printed objects, and websites.
*Computing innovations* are innovations which include a computer program as a
core part of its functionality.  GPS and digital maps are examples of
computing innovations that build upon the early non-computer innnovation of
map making.


## Development Process 

![Software Development Life Cycle](resources/SDLF.svg)


## Errors

Four types of programming errors need to be understood:

1. Syntax errors
2. Runtime errors
3. Logic errors
4. Overflow errors

Which type of error is this?
```
for i in range(10)
    print(i)
```
* Syntax error


Which type of error is this?
```
# Add the numbers from 1 to 10 and print the result
total = 0
for num in range(10)
    total += num 
print(total)
```
* syntax error


Which type of error is this?
```
nums = [3, 5, 8, 0, 9, 11]
result = 1
for num in nums:
    result = result / num
print(result)
```

* logic error
