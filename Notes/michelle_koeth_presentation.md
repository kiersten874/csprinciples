# Presentation Notes/questions

## Edge Computing
* processing on board, not cloud
* collect and process data at the source
* greater and higher quality data
* less central processing
* less network traffic
* data privacy
* specilized hardware

## TinyML
* Tiny ML - tiny machine learning
* machine learning on "the edge"
* runs on low power

## Convolution
* a filter us used to add values of a pixel within an image to its neigboring pixels
* the subsquent subsampleing layer reduces the dimensions of these feature maps

