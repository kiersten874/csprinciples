# Chapter 9 - Repeating Steps With Strings


## Using repition with steps
Learning Objectives:
* Show how the accumulator pattern works for strings.
* Show how to reverse a string.
* Show how to mirror a string.
* Show how to use a while loop to modify a string.

* Python has the ability to play with words or strings
   * A Python for loop knows how to step through letters and addition (+) appends strings together


5 steps of the acculmator pattern:
1. Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
2. Get all the data to be processed.
3. Step through all the data using a for loop so that the variable takes on each value in the data.
4. Combine each piece of the data into the accumulator.
5. Do something with the result.

example:
![code image](images/ch9code.jpg)


## Reversing Text

* Typing 'letter + newstring' prints the phrase backwards. However if typed 'newstring + letter' it will print in the correct order.

![code image](images/ch9code2.jpg)


## Mirroring text
* In the previous section, it says how adding letter to the front or end. If you add it to both sides, it will mirror the text.
* For example, 'newstring = letter + newstring + letter' will print 'tset a si sihTThis is a test'
* If you want to have something in the middle of a mirror text to only print once, add it in the middle of "" when defining 'newstring' in the beginning.

## Modifying Text
* use 'pos = strname.find("1") to find a part of a text and modify. (strname should be replaced with whatever string name, and 1 should be replaced with whatever you want to replace)
* If you are trying to replace/modify  all of one l letter/number from a phrase, you can use while to modify all of them.
* This is good if you misspelled a word multiple times.

