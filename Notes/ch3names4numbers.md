
 Chapter 3: Names for Numbers

## Assigning a Name

### Objectives
* Understand variable concept
* assign value to variable 
* use assigned values in calcuations
* understand ways students get assignments wrong
* reuse variables across differnt statements

### Variables
* A computer assigns a name with a value by creating a variable.
* Examples of variables: Video game scores, contacts assigned with phone numers,
* A variable is like a box with a label on it. 
* A variable's value can be anything that can be stored on the computer's memory.
* Setting a variable's assignment is called an assignment.
* A statement, for example: b=6, b refers to space in the computer's memory, and 6 is its assigned value.If  you wanted to change b's value from 6 to 8, you can do so by writing 'b=8'.

### Legal Variable Names
* Must stat with and a letter or underscore.
* It can contain numbers, but not as the character.
* Can't be a python keyword (elif, else, if, etc)
* case matters. ('info' doesn't equal 'Info')
* you can't have a space in a variable name


## Expressions

* The right hand side of the assignment can be an arthimetic expression, not just a value.
* The modulo operator (%) returns the remainder when you divide the first number by the second. (ex: 5%2, 2 can go into 5 twice with 1 left over, so 5%2=1) 


## Summary of Expression Types
* + Addition
* * Multipaction
* / division
* // floor division
* % Modulo
* - Negation

## How Expressions are Evaulated
The order expressions are evaulted are from highest to lowest precedent. If multiple symbols have the same precedent it goes from left to right, just like in math. 


## Driving from Chicago to Dallas
* Using CodeLens to trace through what a computer would do to excute a program (and what it looks like) that calcualtes the amount of gallons of gas it takes to drive from Chicago to Dallas.
* When tracing, you can check values.

* The function print can input which value will be displayed.
* You can print strings with the print function. Strings are sequences of charcters in a pair of quotes. Using 'print', it will show exactly whats in these strings. (Ex:   print("My favorite color is green."))



## Following the ketchup ooze

* Code to find how long it will take ketchup to ooze own a tilted 4 foot table at a speed of 0.028 mph.

![Ketchup Code screenshot](images/ketchupcodescreenshot.jpg) 

* Results:

![Ketchup results screenshot](images/ketchupresultsscreenshot.jpg)



## Walking through an assignment more generally

* Sequence of statements in a program is very important
* If a variable equals one thing, it doesn't always stay that way, it can be replaced. (For examble, if you wrote a=4, a=8, print(a), it would print 8.

## Figuring Out An invoice

* Variables can be used to solve problems the same way a spreadsheet would. 
* You can reuse variables after you are done computing the total of the line.
* When making variables, use names that make sense


