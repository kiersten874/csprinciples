# Introduction to Networking Chapter 1
   
![alt text](networklayers.png)

## Commincating at a distance
* early telephone systems were created using wires, microphones and speakers. However, only 2 lines could be connected at a time and not everyone was connected to each other, so they were connected to a operator who would connect the two people so they could talk, then disconnect them when they are done. This orked well for local lines. 
* However, people wanted to talk to people af away and running A LOT of  wires that are 100+ miles long to one central office is extremely impractical. So instead, there was just a multiple central offices connected to each other. However, this still was expensive and nott very practical so long distance calls were expensive.

## Computers communicate diffrently
* computers send short, meduim, and large messages to each other.
* In the early days of computers, computers comminicated with eachother through wires. they would eait their to turn to send/recieve a message. 
* if computers were in the same town, they would usually lease telephone wires to have the computers commuinicate. this could also be done if the computers are in differnt towns, but nit would be way more expensive.

## Early Wide Area Store-and-Forward Networks
* in the 70s and 80s, universities wanted to send message s long distances, but that was very expesnsive, so instead they sent it to a local computer, which sent it to another local computer, and so forth until it reached the desired destination. This could take a while. This is multi hopping networks.

## Packets and routers
* Packets - breaking messages apart in to smaller 'packets' so they could multi hop networks quicker. 
* This reduced the storage needed in the intermediate computers.
* Router - A specialized computer that is designed to receive incoming packets on many links and quickly forward the packets on the
best outbound link to speed the packet to its destination. 
![image](packetsandrouters.png) 

## Putting it all together
* Combining all this toghether, we can understand todays computers and networks. 

# Chapter 2: Network Architecture

## The Link Layer
* The link layer is responsible for connecting your computer to its
local network and moving the data across a single hop
* most common link layer technolgy is wirelessly networking. When using a wireless device, you need toconnect to a nearby cellphone tower within a few miles, or if using wifi, you need to be within a certain distance from the base station.
* The link layer needs to solve problems like how to encode and send data, and agreeing on how to cooperate with other computers. 
* CSMA/CD - how computers sense if another computer is trying to send something so they can wait their turn. 
![link layer image](linklayer.png)

## The Internetwork Layer
* your packet needs a source and destination adress
* as your packet hops, the computers dont know the excat location of the desnation, so it guesses. But as it gets closer, more computers will know its precise location.

## The transport layer
* The internetwork layer looks at
a packet’s destination address and finds a path across multiple
network hops to deliver the packet to the destination computer, however sometimes the packet gets lost, delayed, or thediffernt packets arrive out of order.
* when this happens, the destination computer sends a request to resend missing data. 
* the sending computer also stores part of the orginal message sent, and discars them when they know that the destination computer has sucessfully recieved the data.
* "window size" is the amount of data that the sending computer is
allowed to send before waiting for an acknowledgement.  

## The application layer
* Each application is generally broken into two halves. One half of
the application is called the “server”. tion
computer and waits for incoming networking connections. The
other half of the application is called the “client”.
* we must also define an “application protocol”
that describes how the two halves of the application will exchange
messages over the network.

## Stacking the layers 
* the 4 differnts are stacked on top of each other. 

 
# Chapter 3: Link Layer

## Sharing the Air
* The radio in your computer can only send data about 300 meters,
so your computer sends your packets to the router in your home,
which forwards the packets using a link to the rest of the Internet.
* “base station” or “gateway" - he first router that handles your computer’s
packets 
* All computers that are close enough to the base station with their
radios turned on receive all of the packets the base station transmits, regardless of which computer the packet is supposed to be sent to. 
* Things a computer gets sent or "hears" get sent that is not for them is safely ignored.
* every wifi radio has a unique serial number, it is also called the MAC address. It is 48 bit. A MAC address is like a “from” or “to” address on a postcard.
* example of how computer looking for a gateway sends a "message":
    From: 0f:2a:b3:1f:b3:1a
    To: ff:ff:ff:ff:ff:ff
    Data: Who is the MAC-Gateway
    for this network?

    From: 98:2f:4e:78:c1:b4
`    To: 0f:2a:b3:1f:b3:1a
    Data: I am the gateway
    Welcome to my network
* if i gets no replies it will retry or u will not be connected.
* Once your computer receives a message with the MAC address
of the gateway, it can use that address to send packets that it
wants the gateway to forward to the Internet.

## Courtesy and Coordination
* computers must coordinate how they send data. 

### Methods of coordinating
* "carrier sense" - The technique is to first listen for a transmission, and if there is already a transmission in progress, wait until the transmission finishes.
* When two computers "talk over each other" (try to send things at the same time), When the WiFi radios detect a collision or garbled transmission, they compute a random amount of time to wait before retrying the transmission. The rules for computing the random wait are set up to make sure the two colliding stations pick different amounts of time to wait before attempting to retransmit the packet.
* The formal name for the listen, transmit, listen, and wait and retry
if necessary is called “Carrier Sense Multiple Access with Collision
Detection” or CSMA/CD.

## Coordination in Other Link layers
* When the link layer needs to operate more efficently, there is a "token" that indicates when each computer can go, so the computers must wait their turn instead of going when it is "silent"
* The “try then retry” CSMA/CD approach works very well when
there is no data or when low or moderate levels of data are being
sent. But on a token-style network, if there is no data being sent
and you want to send a packet, you still have to wait for a while
before you receive the token and can start transmitting.
* The token approach is best suited when using a link medium
such as as a satellite link or a undersea fiber optic link where
it might take too long or be too costly to detect a collision. The
CSMA/CD (listen-try) is best suited when the medium is inexpensive, shorter distance, and there are a lot of stations sharing the
medium that only send data in short bursts.


# Chapter 5: The Domain Name System

* The Domain Name System lets you access websites by their
domain name like (www.khanacademy.org), so you don’t have
to keep a list of numeric Internet Protocol (IP) addresses like
“212.78.1.25”.
 
* When your computer makes a connection to a system using a
domain name address, the first thing your computer does is look
up the IP address that corresponds to the domain name.

## Allocating Domain Names

*  IP addresses are allocated
based on where you connect a new network to the Internet. Domain names are allocated based on organizations that “own” the
domain name

* At the top of the domain name hierarchy is an organization called the International Corporation for Assigned Network Names and Numbers(ICANN). ICANN chooses the top-level
domains (TLDs) like .com, .edu, and .org and assigns those to
other organizations to manage


## Reading Domain Names
* When we look at an IP address like “212.78.1.25”, the left prefix
is the “Network Number”, so in a sense we read IP addresses
from left to right, where the left part of the IP address is the most
general part of the address and right part of the address is most
specific.

* Example: 212.78.1.25
          Broad -> Narrow

* for domain names, its the oppisite. The last part is the most borad.
* example: school.com,   '.com' is the borad part and its on the right side.

# Chapter 6: The Transport Layer

* The next layer up from the Internetworking layer is the Transport
layer. A key element of the Internetworking layer is that it does
not attempt to guarantee delivery of any particular packet. The
Internetworking layer is nearly perfect, but sometimes packets
can be lost or misrouted.

## Packet Headers
* If you were to look at a packet going across one of many links
between its source and destination computers, you would see a
link header, an IP header, and a Transport Control Protocol (TCP)
header, along with the actual data in the packet.
* The link header is removed when the packet is received on one
link and a new link header is added when the packet is sent out
on the next link on its journey

## Packet Reassembly and Retransmission
* As the destination computer receives the packets, it looks at the
offset position from the beginning of the message so it can put
the packet into the proper place in the reassembled message.
*  Transport layer
easily handles packets that arrive out of order. If it receives a
packet further down a message, it places that packet in a buffer,
keeping track of the fact that there is now a gap in the message
that is being reconstructed.
* To avoid overwhelming the network, the Transport layer in the
sending computer only sends a certain amount of data before
waiting for an acknowledgement from the Transport layer on
the destination computer that the packets were received. This is known as "window size"
* The sending computer adjusts its window size if the response are quick (makes it bigger) or slow(makes it smaller).
* If a packet is lost, it will never arrive at the destination computer
and so the destination computer will never send an acknowledgment for that data. Because the sending computer does not receive an acknowledgment, it quickly reaches the point where it
has sent enough unacknowledged data to fill up the window and
stops sending new packets. 
* This makes each computer wait and At some point, the
66 CHAPTER 6. TRANSPORT LAYER
receiving computer decides too much time has passed and sends
a packet to the sending computer indicating where in the stream
the receiving computer has last received data. When the sending
computer receives this message, it “backs up” and resends data
from the last position that the receiving computer had successfully received.

## The Transport Layer In Operation

* One of the key elements of the Transport layer is that the sending
computer must hold on to all of the data it is sending until the
data has been acknowledged. Once the receiving computer acknowledges the data, the sending computer can discard the sent
data.

## Application Clients and Servers
* The purpose of the Transport layer is to provide reliable connections between networked applications so those applications can
send and receive streams of data. For an application, this is as
simple as asking the Transport layer to make a connection to an
application running on a remote host. We call the application that
initiates the connection on the local computer the “client” and the
application that responds to the connection request the “server”.

##  Server Applications and Ports
* For instance, a web client (a browser) needs to connect to the web
server running on the remote computer. So a client application
not only needs to know which remote computer to connect to, it
also needs to choose a particular application to interact with on
that remote computer.
* port: A way to allow many different server applications to be
waiting for incoming connections on a single computer. Each
application listens on a different port. Client applications make
connections to well-known port numbers to make sure they are
talking to the correct server application.
 
* When a server application starts up, it “listens” for incoming connections on the specified port. Once the server application has registered that it is ready to receive incoming connections, it
waits until the first connection is made.



# Chapter 7: Application Layer

* The application layer is at the top
* The Application layer is where the networked software like web browsers,
mail programs, video players, or networked video players operate.

## Client and Server Applications
* The server portion of the application runs somewhere on the Internet and has the information
that users want to view or interact with
* The client portion of the
application makes connections to the server application, retrieves
information, and shows it to the user
* To browse a web address like www.khanacademy.org, you must
have a web application running on your computer. The web browser on your computer sends a request to connect
to www.khanacademy.org. Your computer looks up the domain
name to find the corresponding IP address for the server and
makes a transport connection to that IP address, then begins to
request data from the server over that network connection. When
the data is received, the web browser shows it to you.
* "web server" - the other end of the connection that is always up and waiting for incoming connections.
* he Transport, Internetwork, and Link layers, along the domain system are like a telephone network that "calls" eachother and has converstions saying what to do.

## Application Layer Protocols

* each pair of network applications needs a set of rules that govern the "conversation". 
* The formal name of the protocol between web clients and web
servers is the “HyperText Transport Protocol”, or HTTP for short.
* Thats why when you put http: at the begaining of a URL u are indicating that u want to retrieve a document using HTTP.
* One of the reasons that HTTP is so successful is that it is relatively
simple compared to most client/server protocols.

## Exploring the HTTP Protocol

* The telnet client application is from "prehistoric" computr times and is still present in most compueters today. The first parameter is a domain name (an IP address would work
here as well) and a port to connect to on that host. We use the
port to indicate which application server we would like to connect
to. (I didnt work when I ran it in the command line though)
* The first parameter is a domain name (an IP address would work
here as well) and a port to connect to on that host. We use the
port to indicate which application server we would like to connect
to.
* Port 80 is where we typically expect to find an HTTP (web)
server application on a host.
* If there is no web server listening
on port 80, our connection will time out and fail. But if there is
a web server, we will be connected to that web server and whatever we type on our keyboard will be sent directly to the server
* Once connected, you can send messages to the server. However, if u dont know the protocols and type something wrong, u get an error message and the connection is closed and u dont get a second chance. 
* if u reply correctly, the host responds with a
series of headers describing the document, followed by a blank
line, then it sends the actual document.

## The IMAP Protocol for Retrieving Mail
* Another common protocol is used
so that a mail application running on your computer can retrieve
mail from a central server
* The messages that are sent by the client and server are not designed to be viewed by an end user so they are not particularly
descriptive. These messages are precisely formatted and are sent
in a precise order so that they can be generated and read by networked computer applications on each end of the connection.

## Writing Networked Applications
* The applications which send and receive data over the network
are written in one or more programming languages.
* the Python programming language,
the key point is that it only takes ten lines of application code to
make and use a network connection.
* By choosing port 80 we are indicating that we want to connect to
a World Wide Web server on that host and are expecting to communicate with that server using the HyperText Transport Protocol.


# Chapter 8: Secure Transport Layer

* In the early days of the internet, there wasnt a need to protect data while it was crossing networks, but later when it became more populaur, security seemed neccessary.
* There are 2 approaches to secuirty: The first makes sure that all of the network hardware (routers and links) is in physically secure locations so it is not possible for someone to sneak in and monitor traffic while it is crossing the
Internet. This isnt very pratical for 100s of routers.
* Optiton 2:the  solution is to encrypt data in your computer before it is sent across its first physical link, and then decrypt the data in the destination computer after it arrives.

## Encrypting and Decrypting Data

* One cypher is the Ceaser cypher, which just shifts each letter by 1. this is easy to crack. 
* we send the ciphertext via the courier or other insecure
transport to the other person. The courier cannot read the message because it appears to be random characters unless you
know the technique used to encode the message.

## Two Kinds of Secrets
* The traditional way to encrypt transmissions is using a shared secret (a password, a sentence, a number) that only the sending
and receiving parties know. 
* Its not a good idea  disprute the secrets bc attackers could be monitoring internet traffic.
* And even worse, the attacker could intercept a message, delay it, then decrypt it, change and re-encrypt it, and send the modified message back on its way. The receiving computer would decrypt the message and never know that it had
been modified by an attacker while in transit.
* The solution to this problem came in the 1970s when the concept of asymmetric key encryption was developed.
* The idea of asymmetric key encryption is that one key is used to encrypt the
message and another key is used to decrypt it. The computer that
will be receiving the encrypted data chooses both the encryption
key and decryption key. Then the encryption key is sent to the
computer that will be sending the data. The sending computer
encrypts the data and sends it across the network. The receiving
computer uses the decryption key to decrypt the data.
* The encryption key is called the "public" key and the decreption key is the "private" key.
* The whole process is designed so that if an attacker has the public
key (which was sent unencrypted) and the encrypted text, it is
virtually impossible to decrypt the encrypted data.

##  Secure Sockets Layer
* Because secruity was adding 20 yrs after the internet, they had to not break any internet protocols to add secruity.  Their solution was to add an optional partial layer between the Transport layer and the Application layer.
* This partial layer is SSL (secure sockets layer) or Transport Layer Security (TLS).
* When an application requested that the Transport layer make a
connection to a remote host, it could request that the connection either be encrypted or unencrypted. If an encrypted connection was requested, the Transport layer encrypted the data before
breaking the stream into packets.
* This meant that the Transport layer, Internetwork layer, and physical (link) layers could still perform exactly the same way whether the packets were encrypted or non-encrypted. The applications making the connections were
also spared the details of how encryption and decryption worked.

## Encrypting Web Browser Traffic
*  Web browsers use the URL convention
of replacing “http:” with “https:” to indicate that the browser is
to communicate with the web server using the Secure Transport
Layer instead of the unencrypted Transport layer. Your browser
will usually show a “lock” icon in the address bar to let you know
that you are communicating with a secure web site.

## Certificates and Certificate Authorities
* While public/private key encryption works to allow the distribution
of encryption keys across insecure networks and the use of those
keys to encrypt transmissions, there is still a problem of knowing
if the public key that you have received when you connected to a
server is really from the organization it claims to be from.
* So your computer needs to know who the key is actually coming
from. This is achieved by sending you a public key that is digitally signed by a Certificate Authority (CA). When your computer
or browser is initially installed, it knows about a number of wellknown certificate authorities. 
