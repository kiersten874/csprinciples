 ASCSP Exam
## How is the APCSP exam scored? How many of the 70 multiple choice questions do I need to get correct to earn a 5, 4, or 3 on the exam? What is the Create Performance Task and how is it scored?

### Create Perfromance task
The create perfomance task is part of the apcsp exam. You need to create a computer program and write a written response about it and a video.
It is scored by 6 catergories. If you fufill each catergory, you get a point.(Max score is 6/6) 
The 6 catergories are program purpose and function, managing complexiety, procedural abstraction, algorith implementation, and testing. 
This website has a good chart of the scoring guidlines: [website](https://sites.google.com/gilroyunified.org/computerscience/principles-ap/create-task/scoring-guidelines?authuser=0) 
 
### APCSP Exam
The apcsp exam is a multiple choice test with 70 questions. Its questions are part of the 5 big ideas: creative devoplment, data, algorthims and programming, computer systems and networks, and impact of computing.
This website is a score calculater for apcsp exam. It will calculate your score based on your create perfomance task score and your exam score: [website](https://www.albert.io/blog/ap-computer-science-principles-score-calculator/) 

### If you got a 6/6 on the create perrfomance task(amount of correct you need on the test to pass):
* you need  60/70 on your test to get 5.
* You need 51/70 to get a 4
* you need 32/70   

###  If you got a 5/6 on the create performance task:
* you need 65/70 to get a 5.
* you need 56/70 to get a 4.
* you need 37/70 to get a 3.

###  If you get  4/6 on the create perfomance task:
* you need 70/70 to get a 5
* you need 60/70 to get a 4
* you need 42/70 to get a 3

### If you get a 3/6 on the create performance task:
* you can't get a 5
* you need 66/70 to get a 4
* you need 47/70 to 3

### If you get a 2/6 on the create perfotmance task:
* you can't get a 5
* you can't get a 4
* you need 52/70 to get a 3

### If you get 1/6 on the create perfromance task:
* you can't get a 5
* you can't get a 4
* you need 57/70 to get a 3

### If you get 0/6 on the create perfomance task:
* You can't get a 5
* you can't get a 4
* you need 62/70 to get a 3


## On which of the 5 Big Ideas did I score best? On which do I need the most improvement?

I did the best on big idea 2- Data. I need the most improvement on big idea 3- algorithms and programming.

## What online resources are available to help me prepare for the exam in each of the Big Idea areas?

I went past 90 minutes here so I don't have time to finish the last question but I will do it later today or tomorrow.  
