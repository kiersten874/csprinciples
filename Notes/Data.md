# Big Idea 2: Data 


## Main ideas

- Abstractions such as numbers, colors, text, and instructions can be
  represented by binary data ("It's all just bits!").
- Numbers can be converted from one number system to another. Computers use
  the binary number system ("It's all just bits!" ;-).
- **Metadata** helps us find and organize data.
- Computers can process and organize data much faster and more accurately than
  people can.
- [Data mining](https://en.wikipedia.org/wiki/Data_mining), the process of
  using computer programs to sift through data to look for patterns and trends,
  can lead to new insights and knowledge.
- It is important to be aware of bias in the data collection process.
- Communicating information visually helps get the message across.
- **Scalability** is key to processing large datasets effectively and
  efficiently.
- Increasing needs for storage led to the development of data compression
  techniques, both lossless and lossy.


## Vocabulary

- abstraction - a way to reduce complexity and make your program more efficent. 
- analog data - data represented in a physical way.
- bias - when the algorthim makes unfair descions
- binary number system - a number system where everything is represented with 1s and 0s
- bit - a binary digit
- byte - 8 binary digits
- classifying data - process of organizing data into catorgories that makes it easier to read and retrieve data for future use.
- cleaning data - process of fixning incorrect, incomplete data or duplicate data. Involves identifying errors then fixing them.
- digital data - electronic represantation of info in a format that computers can read and understand.
- filtering data - process of choosing a smaller part of your program to anylaze
- information - facts provided about something 
- lossless data compression - restores and rebuilds file data in its original form after the file is decompressed. Ex,if you compress a image this way the quality remains the same.
- lossy data compression -  the data in a file is removed and not restored to its original form after decompression.
- metadata - data that provides info abt other data. summarizes data and makes it easier.
- overflow error - the data type used to store data was not large enough to hold the data
- patterns in data - set of data that follows a reconizlable form
- round-off or rounding error - the difference between an approximation of a number used in computation and its exact
- scalability - measure of a systems ability to function well when it is changed to meet a user need.
