# While and For Loops

## Loops- For a while

### Learning Objectives
* introuce the concept of a while loop (a loop that repeats a body of code while a logical statement is true.)
* introduce concept of a logical statement (A statement that is either true or false)
* introduce infinite loop (a loop that never ends)
* compare and contrast while and for loops

### Example For Loop
* A for loop repeats the body of a loop a known number of times.
* The body of the loop is all the statemnets that follow the for statement (and are indented under it) 
* Indented 4 spaces

### Introducing a While Loop
* One way to repeat statements is using a while loop. A while loop repeats as long as a logical statemnent is true.
* While loops are usually used when you don't know how many times to repeat the code. (For example, when you're playing a game, you don't continue playing once someone has won. You end the game there.)
 
## Infinite Loops
An infinite loop is a loop that continues to go forever, unless forced to stop. For example, if you do "While 1 == 1:" it will print forever because 1 will always be equal to 1.

## Counting With A While Loop
* Increment means to increase the value by one
* We can  use a counter variable that we will increment inside the loop.
* Note that we continue the loop as long as the counter is less than the desired last value plus one.

### Comparing While and For loop

![screenshot of while & for loops comparision](whileforloop.jpg) 

* The first line of the for loop creates the variable counter, and then lists the values from 1-10. It then sets the counter to 1. The body of the loop prints the current value of the counter, then moves to the next number.
* The first line of while loop creates and sets the variable counter to 1. On the second line it then tests if the value is less than 11. If it is, it excutes the body of the loop until it gets to 11. 


## Looping when you don't know when to stop
* The logical expression will be evaluated just before the body of the loop is repeated.


How to find the square root of a number:

![Image of code finding sq root](Sqrootcode.jpg) 

## Nested for loops
* The body of a loop can also include another loop.
* Two differnt ways you can look at a program: looking at the sturcture and looking at what you can understand just by looking at it.
* The formula for calculating the number of times the inner loop executes is the number of times the outer loop repeats multiplied by the number of times the inner loop repeats.
