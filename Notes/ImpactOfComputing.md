# Big Idea 5: Impact of Computing 


## Main ideas

- New technologies can have both beneficial and unintended harmful effects,
  including the presence of bias.
- Citizen scientists are able to participate in identification and problem
  solving.
- The digital divide keeps some people from participating in global and local
  issues and events.
- Licensing of people’s work and providing attribution are essential.
- We need to be aware of and protect information about ourselves online.
- Public key encryption and multifactor authentication are used to protect
  sensitive information.
- Malware is software designed to damage your files or capture your sensitive
  data, such as passwords or confidential information.


## Vocabulary

- aymmetric ciphers - a key that uses a public and private key to protect a message and keep it from unauthorized users.
- authentication - verifying the identity of a user.
- bias - being in favor of one side rather than the other and being close-minded and ingornant to facts.
- Certificate Authority (CA) - a trusted organazation that can issue certificates for websites or other entities.
- citizen science - the collection and analysis of data done by the general public.
- Creative Commons licensing - a license gave by the copyright order that lets anyone use their copyright work.
- crowdsourcing - customers recieving the software and testing it. 
- cybersecurity - protecting from unauthorized digital attacks.
- data mining - process of anylazing, discovering, and predicting.
- decryption - changing encrypted info to its orginial state.
- digital divide - the divide between people who have easy access to computers and the internet and those who do not.
- encryption - a process that changes info so it becomes unreadable.
- intellectual property (IP) - computer progam/code protected from theft/copyright.
- keylogging - activity- monitoring software that can give hackers your info and data.
- malware - sowftware thta will harm your computer.
- multifactor authentication - and method to verify the user using 2 or more verifaction factors.
- open access - free access to info that can be seen by anyone.
- open source - code that can be seen, edited, or added to by anyone.
- free software - software that gives its commuinity the freedom to run, copy, distrupte and change the software.
- FOSS - refers to software that is free software and open source.
- PII (personally identifiable information) - a reprensation of info that allows the idenity of the person tobe reasonally used by those it permits? (idk, kinda confused on this one.)
- phishing - emails from fake companies or impersonating compaanies to try to get personal info such as credit card number, passwords, etc. 
- plagiarism - copying someone else's work and claiming it to be your own.
- public key encryption - a method of encrypting using a public and private key.
- rogue access point - an acesses point installed without the network's permission
- targeted marketing - targeting people basedon their demagraphics for marketing.
- virus - a malicous code that can corrupt or destroy a computer.
