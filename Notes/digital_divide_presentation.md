# Access to Information: Digital Divide By Kiersten S. & Mayah M.


# What is Digital Divide?

The gap between people who have easy access to computers and the internet and those who do not. This is due to differences in where people live, their income, race and ethnicity. 

## Statistics
*  1/3 of all people suffer from digital divide.

* In 2017 the test company ACT conducted a survey on a random group of students. They were asked various questions about their access to technology at home and at school. More specifically the number of devices they have access to, the reliability of those devices and how often they can be used for school-related activities. The results of this experiment were widespread.

## Data Charts

![Chart of rural vs urban areas](images/digitaldividechart.png)
![chart americans income affecting access to tech](images/income_digitaldivide.jpeg)

## Importance 
*  It can negatively affect education. Research shows that people that do not have access to the internet have on average 0.4 lower GPA than those who do. Digital divide also limits a student’s  access to education.
* People have less access to quality job opportunities. 
* Digital divide also is affecting people’s health. 37% of adults use the internet to manage physical health, which is not an option for those who do not have access to the internet. 83% of general practitioners expressed concern for their patients who did not have access to the internet. 


### World Map of Access to the Internet
![image of world map](images/worldmap_digitaldivide.jpeg)

### US Map of Access to the Internet
![image of us map](images/digitaldividemapus.jpg)


## Sources:

[Source 1](https://www.weforum.org/agenda/2022/05/how-to-counter-the-global-digital-divide/)

[Source 2](https://www.edsurge.com/news/2021-01-27-the-digital-divide-has-narrowed-but-12-million-students-are-still-disconnected#:~:text=Why%20the%20Digital%20Divide%20Matters,their%20peers%20with%20reliable%20access.com)

[Source 3](https://www.goodthingsfoundation.org/insights/digital-exclusion-and-health-inequalities/) 

[Source 4](https://www.act.org/content/dam/act/unsecured/documents/R1698-digital-divide-2018-08.pdf)


