# CPT Scoring Guidelines Summary

General Scoring Notes:
*  Responses should be evaluated solely on the rationale provided.
*  Responses must demonstrate all criteria, including those within bulleted lists, in each row to earn the point for that row.
*  Terms and phrases defined in the terminology list are italicized when they first appear in the scoring criteria.
* Each section is worth 0-1 points. 

## Program Purpose and Function 
* The video demonstarates the programs input, output and program functionality.
* The written responses dscribe the purpose of the program, describes the functionallity, and describes the input and the output of the program. 
* The purpose must address the problem being solved or creative intrest beoing pursued in the program. 
* The video must show the program running,

## Data Abstraction
* The written response must include two program code segements. One that shows data being stored in a collection type(for example, a list). The other that the list fulfilling the programs purpose.
* The written response must identify the variabel being used to repersent the list(or other collection type).
* The written response must the data contained inside the list.
* The list cannot be one element.
* The list must fulfill the progams purpose.

## Managing complexity
* The written response must include of segment of code from the program that shows how a list manages complexity.
* Explains how the list manages the complexity
* The list must be relevent.
* The list must be accurate and consistent with the program.
* The list must make to code easier to mantain. 
* The list used in the written response must be the one used in your program and must have the right explanation.

## Procedral Abstraction
* The written response must include a code segemnt showing at least one procedure that has at least one paramenter with an effect on the code's functionality.
* It should also include a code segment in the written response that shows where the procedure is being called.
* The written response must also describe how the procedure contribuates to the functionallity of the program. You will not get the point if the procedure doesn't contribuate to the overall functionallity the program.

## Algorithm Impletmation 
* The written response must include code segemnets of an algorithm with sequencing, selection, and iteration.
* The written response should explain in detailed steps how the algorithm works, and someone should be able to re-create it.
* The algorithm must affect the outcome of the code.

## Testing
* The written response must decribe 2 calls to the selceted procedure.
* The written response must escrbe the conditions being tested by each call, and identifies the result of each call.
* Must be accurate  
