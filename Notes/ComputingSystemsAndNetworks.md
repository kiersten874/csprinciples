# Big Idea 4: Computing Systems and Networks 


## Main ideas

- Built-in **redundancy** enables the Internet to continue working when parts
  of it are not operational.
- Communication on the Internet is defined by **protocol** such as
  TCP/IP that determine how messages to send and receive data are formatted.
- Data sent through the Internet is broken into **packets** of equal size.
- The Internet is **scalable**, which means that new capacity can be quickly
  added to it as demand grows.
- The **World Wide Web** (WWW) is a system that uses the Internet to share
  web pages and data of all types.
- Computing systems can be in **sequential**, **parallel**, and **distributed**
  configurations as they process data.


## Vocabulary

- Bandwidth - maxium amount of data transmitted over the internet in a given amount of time. 
- Computing device - a functional unit that can perform computations w/o humman assistance, for example computers, smart phones, flash drives, etc 
- Computing network - interconnected computing devices that can exchange/share data.
- Computing system - a system of one or more computers w/ accosiated software and common storage.
- Data stream - data genarted contiously by many sources
- Distributed computing system - method of making multiple computers work together to solve a problem.
- Fault-tolerant - ability of a system to continue when something fails
- Hypertext Transfer Protocol (HTTP) - applicated protocol for systems to allow users to on www
- Hypertext Transfer Protocol Secure (HTTPS) - secure verison of http
- Internet Protocol (IP) address - a unique string of charcters that identifies each computer 
- Packets - small segment of larger data 
- Parallel computing system - study/design to make multiple computing systems solve a problem.
- Protocols - offical procedure
- Redundancy- being no longer useful
- Router
