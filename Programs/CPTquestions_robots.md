# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program

The purpose of the program is a game for entertainment purposes.


2. Describes what functionality of the program is demonstrated in the
   video.

The video shows how the program functions by showing how you the player moves and the robot follows, and how things can collide.


3. Describes the input and output of the program demonstrated in the
   video.

The video shows how the input is the person moving their player, and the output is the game ending ever they've won/lost.


## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.


This list stores the robots.


![defplacerobotsimg](defplacerobots.png) 




2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.

![screenshot](robotlistinuse.png)



## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response

Numbots

2. Describes what the data contained in the list represent in your
   program
robots


3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list

With numbots you don't have to type each individual robot, you can store them all with one variable.


## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration


This procedure moves the player and is nesscary for the program.


![def move player image](defmoveplayer.png)

2. The second program code segment must show where your student-developed
   procedure is being called in your program.

This procedure is being called while the program is not finished.

![check collisons img](checkcollisons.png)



## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program

The procedure defines how the player moves when a key is pressed. It contributes to the program by making you be able to move your player.


4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.


The procedure defines the variable ``` move_player ``` by adding a direction foreach key to move in a certain direction by adding or subtracting from the x and y axis which are stored in variables called ``` player_x ``` and ``` player_y ```.


## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.

 First call:

``` check_collisons() ```


 Second call:

``` move_robots() ```


2. Describes what condition(s) is being tested by each call to the procedure

 Condition(s) tested by the first call:

If anything has collided.


 Condition(s) tested by the second call:

how the the robot moved based on where the player moved.


3. Identifies the result of each call

 Result of the first call:

program will end if there is collisons.


 Result of the second call:

robots will follow player.

