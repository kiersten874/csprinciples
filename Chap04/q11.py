# exercise 11

#Modify line 6 to print: “You can order 40.0 wings when you have 5 people who can each spend 4 dollars and wings cost 0.5 each.”


num_people = 5
amount_per_person = 4
price = 0.5
total = num_people * amount_per_person
num_wings =  total / price
print(f"You can order {num_wings} wings when you have {num_people} people who can each spend {amount_per_person} dollars and wings cost {price} each.")
