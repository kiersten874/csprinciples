# excersise 13

#Combine lines 4 and 5 in the code below to print: “270 is 4.0 hours and 30 minutes.”

total_minutes = 270
num_minutes = total_minutes % 60
num_hours = (total_minutes - num_minutes) / 60
print(f"{total_minutes} is {num_hours} hours and {num_minutes} minutes.")
