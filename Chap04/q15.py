# exercise 15

# Complete the calculations on lines 2 and 4 and enter the items to be printed on line 5 to print the number of miles you can drive if you have a 10 gallon gas tank and are down to a quarter of a tank of gas and your car gets 32 miles per gallon. It should print: “You can go 80.0 miles.”

tank_capacity = 10
num_gallons = 2.5
miles_per_gallon = 32
num_miles = num_gallons * miles_per_gallon
print(f"You can go {num_miles} miles.")
