#Exercise 18: Write code to get the input of a user’s first name, then get only the first letter of their name, and print that letter lowercase.

name = input("What is your name?").lower() 
print(name[0])
