# exercise 19: Write the code below to calculate and print how many months it will take to save $200 if you earn $20 a week. It should print: “It will take 2.5 months to earn 200 if you make 20 dollars a week.”

wkly_wage = 20
mnthly_wage = wkly_wage * 4
time_until_200 = 200 / mnthly_wage
print(f"It will take {time_until_200} months to earn $200 if you make {wkly_wage} dollars a week.")

