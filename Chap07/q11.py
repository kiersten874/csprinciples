#The code below sums the even numbers from 0 to 20 inclusive. Turn it into a function sum_evens(to_num) that calculates the sum of the even numbers from 0 to the value of to_num passed to the function. Return the sum from the function. Call the function inside print to see the result.

def sum_evens(to_num):
   sum = 0  
   numbers = range(0, to_num, 2)
   for number in numbers:
      sum = sum + number
   return sum   
print(sum_evens(20))
