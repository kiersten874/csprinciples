
"""
>>> sum_list([1, 1, 1])
1
>>> sum_list([10, 1, 1])
12
>>> sum_list([10, 3, -1, 8])
20
"""


def sum_list(num_list):
   sum = 0
   for number in num_list:
      sum = number + sum
   return sum


"""
>>> mult_list([1, 1, 1])
1
>>> mult_list([10, 2, 3])
0
>>> mult_list([10, 3, -1, 8])
-240
"""

def mult_list(num_list):
   product = 1
   for number in num_list:
      product = number * number
   return number

def factorial(n):

"""
>>> factorial(5)
120
"""
def factorial(n):
   f = 1
   for i in range(1, number + 1):
       f = f * i
       return i


if __name__ == "__main__":
   import doctest

    doctest.testmod()
