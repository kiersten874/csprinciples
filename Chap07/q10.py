# Write code that prints the square of each number 1 through 10 in the format “1 * 1 = 1”, etc.

list = range(1, 11)
for number in list:
    square = number * number
    print(f"{number} * {number} = {square}")

