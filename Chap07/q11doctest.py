

"""
>>> q11(range(0, 21, 2))
110
"""

def q11(numbers):
   sum = 0
   numbers = range(0, 21, 2)
   for number in numbers:
      sum = sum + number
   return sum

if __name__ == "__main__":
        import doctest
        doctest.testmod()
