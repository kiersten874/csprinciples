# Fill in the missing values in the function sum_list so that it returns the sum of all the numbers in the parameter num_list. Create a test list and call
# it sum_list passing it your list



def sum_list(num_list):
    total = 0                # Start with additive identity
    for n in num_list:             # Loop through each number
        total = total + n         # Accumlate the total
    return total

my_nums = [1,2,3]                 # Create a list of numbers
print(sum_list(my_nums))          # Print the call result to test
